function firebaseinit(){
    let config = {
        apiKey: "AIzaSyCk9tCZvvQ4e2ztVtEezW_lebc79PXaTbc",
        authDomain: "washu-usedbook.firebaseapp.com",
        databaseURL: "https://washu-usedbook.firebaseio.com",
        projectId: "washu-usedbook",
        storageBucket: "washu-usedbook.appspot.com",
        messagingSenderId: "988288386198"
    };
    firebase.initializeApp(config);
}
firebaseinit();
function updateuser(user){
    refreshui(user);
    var db = firebase.firestore();
    let photourl="";
    if (user.photoURL!=null){
        photourl=user.photoURL;
    }
    // let phoneNumber="";
    // if (user.phoneNumber!=null){
    //     phoneNumber=user.phoneNumber;
    // }
    db.collection("users").doc(user.uid).set({
        username: user.displayName,
        email: user.email,
        photourl: photourl,
        // phone: phoneNumber

    },{ merge: true })
    .then(function() {
    })
    .catch(function(error) {
        console.error("Error writing document: ", error);
    });
}
function refreshui(user){
    $("#name").text(user.displayName);
    $("#email").text(user.email);
    if(user.photoURL!=null){
        $("#img").attr('src',user.photoURL);
    }
   
}

function logout(){
    firebase.auth().signOut()
    window.location.replace('homepage');
}

function myselling(){
    window.location.replace('myselling');
}
function homepage(){
    window.location.replace('homepage');
}

function sellbook(){
    window.location.replace('sellbook');
}
function editprofile(){
    window.location.replace('editprofile');
}
