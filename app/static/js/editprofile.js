function firebaseinit() {
    let config = {
        apiKey: "AIzaSyCk9tCZvvQ4e2ztVtEezW_lebc79PXaTbc",
        authDomain: "washu-usedbook.firebaseapp.com",
        databaseURL: "https://washu-usedbook.firebaseio.com",
        projectId: "washu-usedbook",
        storageBucket: "washu-usedbook.appspot.com",
        messagingSenderId: "988288386198"
    };
    firebase.initializeApp(config);
}
firebaseinit();

let localimgref = "";
function saveprofile() {
    let username = $("#name").val();
    // let phonenumber = $("#phonenumber").val();
    let major = $("#major").val();
    let grade = $("#grade").val();
    if (username == "") {
        alert("Username cannot be empty");
        return;
    }
    const userinfo = {
        username: username,
        // phone: phonenumber,
        major: major,
        grade: grade

    }
    updateuser(userinfo);

}

function cancel() {
    window.location.replace('account');
}

function updateuser(userinfo) {
    let db = firebase.firestore();
    var user = firebase.auth().currentUser;

    user.updateProfile({
        displayName: userinfo.username,
        // phoneNumber: userinfo.phone
        
    }).then(function () {
        // Update successful.
        // console.log(user.phoneNumber);
    }).catch(function (error) {
        // An error happened.
    });
    db.collection("users").doc(firebase.auth().currentUser.uid).update(userinfo).then(function () {
        
        // return;
        db.collection("book").where("email", "==", firebase.auth().currentUser.email).get()
        .then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                // doc.data() is never undefined for query doc snapshots
                db.collection("book").doc(doc.id).update(
                    { "username": userinfo.username }
                )
                    .catch(function (error) {
                        console.error("Error writing document: ", error);
                    });
            });
        })
        if (localimgref != "") {
            updateuserimg();
        }
        else {
            window.location.replace('account');
        }

    })
        .catch(function (error) {
            console.error("Error writing document: ", error);
        });
    

}

function changeimgref(file) {
    localimgref = file;
}

function updateuserimg() {
    var metadata = {
        contentType: 'image/jpeg',
    };
    let storage = firebase.storage().ref("user/" + firebase.auth().currentUser.uid + "/userphoto");
    var uploadTask = storage.put(localimgref, metadata);
    uploadTask.on('state_changed', function (snapshot) {
        // Observe state change events such as progress, pause, and resume
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log('Upload is ' + progress + '% done');
        switch (snapshot.state) {
            case firebase.storage.TaskState.PAUSED: // or 'paused'
                console.log('Upload is paused');
                break;
            case firebase.storage.TaskState.RUNNING: // or 'running'
                console.log('Upload is running');
                break;
        }
    }, function (error) {
        // Handle unsuccessful uploads
    }, function () {
        // Handle successful uploads on complete
        // For instance, get the download URL: https://firebasestorage.googleapis.com/...
        const db = firebase.firestore();
        uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
            var user = firebase.auth().currentUser;

            user.updateProfile({
                photoURL: downloadURL,
            }).then(function () {
                // Update successful.
            }).catch(function (error) {
                // An error happened.
            });
            console.log('File available at', downloadURL);
            db.collection("users").doc(firebase.auth().currentUser.uid).update(
                "photourl", downloadURL
            )
                .then(function () {
                    db.collection("book").where("email", "==", firebase.auth().currentUser.email).get()
                        .then(function (querySnapshot) {
                            let i=0
                            for(i=0;i<querySnapshot.docs.length;i++){
                
                                doc=querySnapshot.docs[i];
                                // console.log(doc.id);
                                // doc.data() is never undefined for query doc snapshots
                                db.collection("book").doc(doc.id).update(
                                    { "sellerimg": downloadURL }
                                )
                                    .then(function () {
                                        
                                    })
                                    .catch(function (error) {
                                        console.error("Error writing document: ", error);
                                    });
                            };
                            window.location.replace("account");
                        })
                    
                })
        });

    });


}

function refreshui(user) {
    $("#name").val(user.displayName);
    $("#email").text(user.email);
    if (user.photoURL != null) {
        $("#Uploaded").attr('src', user.photoURL);
    }
    // $("#phonenumber").text(user.phoneNumber);

}