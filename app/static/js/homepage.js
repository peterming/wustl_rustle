function firebaseinit() {
    let config = {
        apiKey: "AIzaSyCk9tCZvvQ4e2ztVtEezW_lebc79PXaTbc",
        authDomain: "washu-usedbook.firebaseapp.com",
        databaseURL: "https://washu-usedbook.firebaseio.com",
        projectId: "washu-usedbook",
        storageBucket: "washu-usedbook.appspot.com",
        messagingSenderId: "988288386198"
    };
    firebase.initializeApp(config);
}
firebaseinit();
var storage = firebase.storage();
var storageRef = storage.ref();
let num_cell = 6;
// console.log($(window).width())
if($(window).width()<600){
    num_cell=2;
}
if($(window).width()<900&&$(window).width()>600){
    num_cell=3;
}
function insertcell(doc, div, i) {
    let cell = div.insertCell(i % num_cell)
    let bookname = doc.data().bookname;
    // let course = doc.data().course;
    let coursecode=doc.data().coursecode;
    let price = doc.data().price;
    // let date = doc.data().date;
    let imagesRef = doc.data().imageref;
    let email=doc.data().email;
    let message=doc.data().message;
    let seller=doc.data().username;
    // console.log(imagesRef);
    let img = $('<img />', {
        class:"bookimg",
        src: "/static/image/no image.png",
        alt: "https://upload.wikimedia.org/wikipedia/commons/6/6c/No_image_3x4.svg"
    });
    img.appendTo($(cell));
    if (imagesRef != "") {
        let imageurl = storageRef.child(imagesRef);
        imageurl.getDownloadURL().then(function (url) {
            // Insert url into an <img> tag to "download"
            // console.log($(img).attr("src"));
            $(img).attr("src",url);

        })
    }
    let bookinfo = document.createElement("div");
    let BookName = document.createElement("p");
    BookName.setAttribute("class", "bookname");
    BookName.textContent = bookname;
    bookinfo.appendChild(BookName);
    let Price = document.createElement("p");
    Price.setAttribute("class", "price");
    Price.textContent = "Price:" + price + "$";
    bookinfo.appendChild(Price);
    // let Course = document.createElement("p");
    // Course.setAttribute("class", "course");
    // Course.textContent = "Subject: ";
    // if (course[0] !== undefined) {
    //     Course.textContent = "Subject: " + course[0];
    // }
    // console.log(coursecode);
    let Coursecode = document.createElement("p");
    Coursecode.setAttribute("class", "coursecode");
    Coursecode.setAttribute("class","coursecode");
    Coursecode.textContent = "Subject: ";
    for(i in coursecode){
        if(coursecode[i]!=undefined){
            Coursecode.textContent=Coursecode.textContent+coursecode[i]+",";
        }
    }

    bookinfo.appendChild(Coursecode);
    // let Date_ = document.createElement("p");
    // Date_.setAttribute("class", "date");
    // Date_.textContent = "Available date: "+date;
    // bookinfo.appendChild(Date_);
    // let Email=document.createElement("p");
    // Email.textContent="Email: "+email;
    // Email.setAttribute("class","email");
    let request_btn=document.createElement("button");
    // request_btn.textContent="Request"
    request_btn.setAttribute('class','button request_btn');
    request_btn.innerHTML="<span>Request </span>";
    $(request_btn).on("click",function(){
        send_email(email,bookname,price,seller);
        update_stat();
        if (firebase.auth().currentUser){
            update_browsing_history(firebase.auth().currentUser,coursecode,bookname)
        }
    })
    let Message=document.createElement("p");
    Message.setAttribute("class","message");
    Message.textContent="Message: ";
    if (message !== undefined) {
        Message.textContent = "Message: " + message;
    }
    // bookinfo.appendChild(Email);
    bookinfo.appendChild(Message);
    cell.appendChild(bookinfo);
    cell.appendChild(request_btn);
    cell.style.width= 100/num_cell+"%";



}

function send_email(email,bookname,price,seller){
    const subject="Book Request for "+bookname;
    const body= "Hi "+seller+",%0D%0A I would like to buy your "+bookname+" for "+price+"$."+"\n Can we schedule a time to meet? ";
    window.open("mailto:"+email+"?subject="+subject+"&body="+body);
}

function update_stat(){
    let db = firebase.firestore();
    db.collection("statistic").doc("stat").update({
        book_request: firebase.firestore.FieldValue.increment(1)
    }).then(function () {
    
    })
      .catch(function (error) {
        console.error("Error writing document: ", error);
      });
}

function update_browsing_history(currentUser,coursecode,bookname){
    let db = firebase.firestore();
    db.collection("statistic").doc(currentUser.uid).set({
    }, { merge: true });
    for (i in coursecode){
        dept=coursecode[i].match(/^[A-z]+/)[0];
        db.collection("statistic").doc(currentUser.uid).update({
            ["coursecode_history."+coursecode]: firebase.firestore.FieldValue.increment(1),
            ["dept_history."+dept]:firebase.firestore.FieldValue.increment(1),
            request_history:firebase.firestore.FieldValue.arrayUnion(bookname)
        }).then(function () {
        
        })
          .catch(function (error) {
            console.error("Error writing document: ", error);
          });
    }
    
}


function update_searching_history(currentUser,coursecode){
    let db = firebase.firestore();
    db.collection("statistic").doc(currentUser.uid).set({
    }, { merge: true });
    
    dept=coursecode.match(/^[A-z]+/)[0];
    db.collection("statistic").doc(currentUser.uid).update({
        ["coursecode_history."+coursecode]: firebase.firestore.FieldValue.increment(1),
        ["dept_history."+dept]:firebase.firestore.FieldValue.increment(1)
    }).then(function () {
    
    })
        .catch(function (error) {
        console.error("Error writing document: ", error);
        });
    
}

function update_searching_history_dept(currentUser,dept){
    let db = firebase.firestore();
    db.collection("statistic").doc(currentUser.uid).set({
    }, { merge: true });
    
    dept=dept.match(/^[A-z]+/)[0];
    db.collection("statistic").doc(currentUser.uid).update({
        ["dept_history."+dept]:firebase.firestore.FieldValue.increment(1)
    }).then(function () {
    
    })
        .catch(function (error) {
        console.error("Error writing document: ", error);
        });
    
}


function insert(doc, target_tbl, i) {
    let booklist = document.getElementById(target_tbl);
    if (i % num_cell == 0) {
        row = booklist.insertRow(i / num_cell);
    }
    row = booklist.rows[Math.floor(i / num_cell)];
    insertcell(doc, row, i);
}




// function loadsubject() {

//     // var select = document.querySelectorAll(".select");
//     var Categories = new Array();
//     Categories['All Categories'] = [''];
//     Categories['Art & Science'] = ["AFRICAN AND AFRICAN-AMERICAN STUDIES", "CHEMISTRY", "EAST ASIAN STUDIES", "JAPANESE", "MIND, BRAIN, AND BEHAVIOR", "PHYSICS", "RUSSIAN", "AMERICAN CULTURE STUDIES", "CHILDREN'S STUDIES", "ECONOMICS", "GERMAN", "JEWISH, ISLAMIC AND MIDDLE EAST STUDIES", "MOVEMENT SCIENCE", "POLITICAL ECONOMY", "RUSSIAN STUDIES", "ANTHROPOLOGY", "CHINESE", "EDUCATION", "GREEK", "KOREAN", "MUSIC", "POLITICAL SCIENCE", "SOCIOLOGY", "ARABIC", "CLASSICS", "ENGLISH LITERATURE", "HEBREW", "LATIN", "NURSING SCIENCE", "PORTUGUESE", "SPANISH", "ARCHAEOLOGY", "COLLEGE WRITING PROGRAM", "ENVIRONMENTAL STUDIES", "HINDI", "LATIN AMERICAN STUDIES", "PRAXIS", "SPEECH AND HEARING", "COMPARATIVE LITERATURE", "EUROPEAN STUDIES", "HISTORY", "LEGAL STUDIES", "PSYCHOLOGY", "ASIAN AMERICAN STUDIES", "DANCE", "FILM AND MEDIA STUDIES", "LINGUISTICS", "PHILOSOPHY", "RELIGION AND POLITICS", "URBAN STUDIES", "BIOLOGY AND BIOMEDICAL SCIENCES", "DRAMA", "FIRST-YEAR PROGRAMS", "INTERNATIONAL AND AREA STUDIES", "MATHEMATICS AND STATISTICS", "PHILOSOPHY-NEUROSCIENCE-PSYCHOLOGY", "RELIGIOUS STUDIES", "WOMEN, GENDER, AND SEXUALITY STUDIES", "CENTER FOR THE HUMANITIES", "EARTH AND PLANETARY SCIENCES", "FRENCH", "ART HISTORY"].sort();
//     Categories['Engineering & Applied Science'] = ["BIOMEDICAL ENGINEERING", "COMPUTER SCIENCE AND ENGINEERING", "ELECTRICAL AND SYSTEMS ENGINEERING", "ENERGY, ENVIRONMENTAL AND CHEMICAL ENGR", "GENERAL ENGINEERING", "MECHANICAL ENGINEERING"].sort();
//     Categories['Architecture'] = ["ARCHITECTURE"];
//     Categories['LAW School'] = ["LAW"];
//     Categories['Medicine'] = ['ANATOMY AND NEUROBIOLOGY', 'ANESTHESIOLOGY', 'GENETICS', 'NEUROLOGY', 'OBSTETRICS AND GYNECOLOGY', 'PATHOLOGY', 'PEDIATRICS', 'OTOLARYNGOLOGY', 'RADIOLOGY', 'SURGERY'].sort();
//     Categories['Business'] = ["MANAGEMENT", "ACCOUNTING", "ADMINISTRATION", "MANAGERIAL ECONOMICS", "DATA ANALYTICS", "MARKETING", "FINANCE"].sort();
//     Categories['Design & Arts'] = ["DESIGN & VISUAL ARTS"];
//     Categories['Public Health & Social Work'] = ["SOCIAL STUDIES"];
//     Categories['Extracurricular Reading'] = ['LITERATURE & FICTION', 'CLASSIC', 'CHILDREN', 'COOKBOOKS', 'HEALTH & FITNESS', 'ROMANCE', 'HISTORY', 'SCIENCE FICTION & FANTASY', 'MYSTERY', 'RELIGION'].sort();

//     var x = $("#school").val();
//     console.log(x);


//     $("#subject").html("");


//     for (let i = 0; i < Categories[x].length; i++) {
//         option = new Option(Categories[x][i], Categories[x][i]);
//         $("#subject").append(option);


//     }
// }

function loadbooklist() {
    const db = firebase.firestore();
    //get book info
    let i = 0;
    let bookRef = db.collection("book");
    bookRef.orderBy("price")
        .get().then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                insert(doc, "booklist", i);
                i++;
            });
        })
    // $("#school").change(loadsubject);
}

function search(query_){
    console.log(query_)
    let coursecode=query_.toUpperCase().trim().replace(/\s/g, '').match(/^[A-Z]+([0-9]+)/);
    let dept=query_.toUpperCase().trim().replace(/\s/g, '').match(/^[A-Z]+/);
    if(coursecode){
        query=coursecode[0];
        bookRef.where("coursecode", "array-contains", query).where("price",">",0).orderBy("price")
        .get().then(function (querySnapshot) {
            // console.log(firebase.auth().currentUser);
            if (firebase.auth().currentUser){
                // console.log(query_);
                update_searching_history(firebase.auth().currentUser,query);
            }
            if(querySnapshot.size==0){
                const table=document.getElementById("booklist");
                table.innerHTML="<h1>No result found<h1><h1> You can visit<h1> <a href= \"https://www.bkstr.com/washingtonstore/shop/textbooks-and-course-materials\">Campus Store<\/a>";
                return;
            }
            querySnapshot.forEach(function (doc) {
                insert(doc, "booklist", i);
                i++;
            });
        })
    }
    else if(dept){
        query=dept[0];
        bookRef.where("dept", "array-contains", query).where("price",">",0).orderBy("price")
        .get().then(function (querySnapshot) {
            // console.log(firebase.auth().currentUser);
            if (firebase.auth().currentUser){
                // console.log(query_);
                update_searching_history_dept(firebase.auth().currentUser,query);
            }
            if(querySnapshot.size==0){
                const table=document.getElementById("booklist");
                table.innerHTML="<h1>No result found<h1><h1> You can visit<h1> <a href= \"https://www.bkstr.com/washingtonstore/shop/textbooks-and-course-materials\">Campus Store<\/a>";
                return;
            }
            querySnapshot.forEach(function (doc) {
                insert(doc, "booklist", i);
                i++;
            });
        })
    }
    else{
        query="";
    }
    
    if (query == "") {
        loadbooklist();
    }
    
    else{
        
        
        
    }
}





