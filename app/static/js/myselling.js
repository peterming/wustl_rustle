function firebaseinit() {
    let config = {
        apiKey: "AIzaSyCk9tCZvvQ4e2ztVtEezW_lebc79PXaTbc",
        authDomain: "washu-usedbook.firebaseapp.com",
        databaseURL: "https://washu-usedbook.firebaseio.com",
        projectId: "washu-usedbook",
        storageBucket: "washu-usedbook.appspot.com",
        messagingSenderId: "988288386198"
    };
    firebase.initializeApp(config);
}
firebaseinit();
var storage = firebase.storage();
var storageRef = storage.ref();
let num_cell = 6;
// console.log($(window).width())
if($(window).width()<600){
    num_cell=2;
}
if($(window).width()<900&& $(window).width()>600){
    num_cell=3;
}
function insertcell_seller(doc, div, i) {
    let cell = div.insertCell(i % num_cell)
    let bookname = doc.data().bookname;
    // let course = doc.data().course;
    let price = doc.data().price;
    // let date = doc.data().date;
    let imagesRef = doc.data().imageref;
    // let email=doc.data().email;
    let message = doc.data().message;
    let coursecode=doc.data().coursecode;
    // console.log(imagesRef);
    let img = $('<img />', {
        class:"bookimg",
        src: "/static/image/no image.png",
        alt: "https://upload.wikimedia.org/wikipedia/commons/6/6c/No_image_3x4.svg"
    });
    img.appendTo($(cell));
    if (imagesRef != "") {
        let imageurl = storageRef.child(imagesRef);
        imageurl.getDownloadURL().then(function (url) {
            // Insert url into an <img> tag to "download"
            // console.log($(img).attr("src"));
            $(img).attr("src",url);

        })
    }
    let bookinfo = document.createElement("div");
    let BookName = document.createElement("p");
    BookName.setAttribute("class", "bookname");
    BookName.textContent = bookname;
    bookinfo.appendChild(BookName);
    let Price = document.createElement("p");
    Price.setAttribute("class", "price");
    Price.textContent = "Price:" + price + "$";
    bookinfo.appendChild(Price);
    // let Course = document.createElement("p");
    // Course.setAttribute("class", "course");
    // Course.textContent = "Subject: ";
    // if (course[0] !== undefined) {
    //     Course.textContent = "Subject: " + course[0];
    // }

    // bookinfo.appendChild(Course);
    // console.log(coursecode);
    let Coursecode = document.createElement("p");
    Coursecode.setAttribute("class", "coursecode");
    Coursecode.textContent = "Subject: ";
    for(i in coursecode){
        if(coursecode[i]!=undefined){
            Coursecode.textContent=Coursecode.textContent+coursecode[i]+",";
        }
    }

    bookinfo.appendChild(Coursecode);
    // let Date_ = document.createElement("p");
    // Date_.setAttribute("class", "date");
    // Date_.textContent = "Available date: " + date;
    // bookinfo.appendChild(Date_);
    let button = document.createElement("button");
    button.setAttribute("value", doc.id);
    button.setAttribute("class", "button");
    $(button).on('click', function () {
        deletebook(this);
    });
    button.innerHTML = "<span>Delete </span>";
    // let Email=document.createElement("p");
    // Email.textContent="Email: "+email;
    // Email.setAttribute("class","email");
    let Message = document.createElement("p");
    Message.setAttribute("class", "message");
    Message.textContent = "Message: ";
    if (message !== undefined) {
        Message.textContent = "Message: " + message;
    }
    // bookinfo.appendChild(Email);
    bookinfo.appendChild(Message);
    cell.appendChild(button);
    cell.appendChild(bookinfo);
    cell.style.width= 100/num_cell+"%";


}
function insert_seller(doc, target_tbl, i) {
    let booklist = document.getElementById(target_tbl);
    if (i % num_cell == 0) {
        row = booklist.insertRow(i / num_cell);
    }
    row = booklist.rows[Math.floor(i / num_cell)];
    insertcell_seller(doc, row, i);
}

function deletebook(button) {
    let docref = $(button).val();
    let imgref = "";
    // console.log(docref);
    let db = firebase.firestore().collection("book").doc(docref);
    db.get().then(function (doc) {
        if (doc.exists) {
            imgref = doc.data().imageref;
            // console.log(imgref);
            if (imgref != "") {
                let storage = firebase.storage().ref().child(imgref);
                storage.delete().then(function () {
                    // window.location.replace('myselling');
                });

            }

            db.delete().then(function () {
                window.location.replace('myselling');
            })



        } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
        }
    }).catch(function (error) {
        console.log("Error getting document:", error);
    });




}