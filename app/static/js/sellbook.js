function firebaseinit() {
  let config = {
    apiKey: "AIzaSyCk9tCZvvQ4e2ztVtEezW_lebc79PXaTbc",
    authDomain: "washu-usedbook.firebaseapp.com",
    databaseURL: "https://washu-usedbook.firebaseio.com",
    projectId: "washu-usedbook",
    storageBucket: "washu-usedbook.appspot.com",
    messagingSenderId: "988288386198"
  };
  firebase.initializeApp(config);
}
firebaseinit();

let localimgref="";
function publish() {
  let username = firebase.auth().currentUser.displayName;
  let email = firebase.auth().currentUser.email;
  let sellerimg = firebase.auth().currentUser.photoURL;
  let bookname = ($("#name").val()).trim();
  // let isbn = $("#ISBN").val();
  let price = parseInt($("#Price").val());
  let date = $("#Date").val();
  let message = $("#Message").val();
  
if($("#coursecode").val().split(",").map(item => item.toUpperCase().trim().replace(/\s/g, '').match(/^[A-z]+([0-9]+)/))[0]==null){
  alert("Must enter a course code");
  return;
}

  let coursecode = $("#coursecode").val().split(",").map(item => item.toUpperCase().trim().replace(/\s/g, '').match(/^[A-z]+([0-9]+)/)[0]);
  console.log(coursecode);

  let dept = $("#coursecode").val().split(",").map(item => item.toUpperCase().trim().replace(/\s/g, '').match(/^[A-z]+/)[0]);
  console.log(dept);
  
  
  let imageref="";
  if(localimgref!=""){
    imageref = "user/" + firebase.auth().currentUser.uid + "/" + bookname;
  }
  
  
  
  if (bookname == "") {
    alert("Book name cannot be empty");
    return;
  }
  if (isNaN(price)) {
    alert("Invalid price");
    return;
  }
  if (coursecode[0] == "") {
    alert("Must enter a course code");
    return;
  }

  
  
  
  

  const bookinfo = {
    username: username,
    email: email,
    // isbn: isbn,
    price: price,
    
    message: message,
    
    sellerimg: sellerimg,
    imageref: imageref,
    bookname: bookname,
    coursecode:coursecode,
    dept:dept

  }
  uploadbook(bookinfo);
  
}

function cancel() {
  window.location.replace('account');
}

function uploadbook(bookinfo) {
  let db = firebase.firestore();
  db.collection("book").doc(firebase.auth().currentUser.uid + "_" + bookinfo.bookname).set(bookinfo).then(function () {
    uploadimg(bookinfo.bookname);
  })
    .catch(function (error) {
      console.error("Error writing document: ", error);
      alert("Error writing document: ", error);
    });

}

function changeimgref(file) {
  localimgref = file;
  
}

function uploadimg(bookname) {
  var metadata = {
    contentType: 'image/jpeg',
  };
  if(localimgref==""){
    window.location.replace('account');
    return;
  }
  let storage = firebase.storage().ref("user/" + firebase.auth().currentUser.uid + "/" + bookname);
  var uploadTask = storage.put(localimgref, metadata);
  uploadTask.on('state_changed', function (snapshot) {
    
    
    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
    console.log('Upload is ' + progress + '% done');
    switch (snapshot.state) {
      case firebase.storage.TaskState.PAUSED: 
        console.log('Upload is paused');
        break;
      case firebase.storage.TaskState.RUNNING: 
        console.log('Upload is running');
        break;
    }
  }, function (error) {
    
  }, function () {
    
    
    window.location.replace('account');
    
    
      
    
  });


}

