let config = {
    apiKey: "AIzaSyCk9tCZvvQ4e2ztVtEezW_lebc79PXaTbc",
    authDomain: "washu-usedbook.firebaseapp.com",
    databaseURL: "https://washu-usedbook.firebaseio.com",
    projectId: "washu-usedbook",
    storageBucket: "washu-usedbook.appspot.com",
    messagingSenderId: "988288386198"
  };
  firebase.initializeApp(config);
  let user = firebase.auth().currentUser;

  if (user) {
    window.location.replace('account');
  } else {
    // No user is signed in.
  }

  let uiConfig = {
    signInFlow:"redirect",
    signInSuccessUrl: 'account',
    signInOptions: [
      // Leave the lines as is for the providers you want to offer your users.
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.EmailAuthProvider.PROVIDER_ID,
      // firebase.auth.PhoneAuthProvider.PROVIDER_ID,
    ],

    // tosUrl and privacyPolicyUrl accept either url string or a callback
    // function.
    // Terms of service url/callback.
    tosUrl: '<your-tos-url>',
    // Privacy policy url/callback.
    privacyPolicyUrl: function () {
      window.location.assign('<your-privacy-policy-url>');
    }
  };

  // Initialize the FirebaseUI Widget using Firebase.
  let ui = new firebaseui.auth.AuthUI(firebase.auth());
  // The start method will wait until the DOM is loaded.
  ui.start('#firebaseui-auth-container', uiConfig);