from django.urls import path

from . import views

urlpatterns = [
    path('',views.homepage,name='homepage'),
    path('homepage',views.homepage,name='homepage'),
    path('signin', views.signin, name='signin'),
    path('account',views.account,name="account"),
    path('search',views.search,name="search"),
    path('myselling',views.myselling,name="myselling"),
    path('sellbook',views.sellbook,name="sellbook"),
    path('editprofile',views.editprofile,name="editprofile"),
    # path('about',views.about,name="about"),
]