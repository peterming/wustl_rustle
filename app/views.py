from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import ensure_csrf_cookie

# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render_to_response

def signin(request):
    return render_to_response('signin.html')

def account(request):
    return render_to_response('account.html')

@ensure_csrf_cookie
def homepage(request):
    return render_to_response('homepage.html')

@csrf_exempt
def search(request):
    query=request.POST.get('coursecode_search')
    print(query)
    return render(request,"search.html",{"query":query})

def myselling(request):
    return render_to_response("myselling.html")

def sellbook(request):
    return render_to_response("sellbook.html")

def editprofile(request):
    return render_to_response("editprofile.html")

def about(request):
    return render_to_response("about.html")